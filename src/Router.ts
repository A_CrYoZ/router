import { IncomingMessage, ServerResponse } from "http";

interface Route<T> {
	path: string;
	method: HTTPMethod;
	handler: Handler<T>;
}

export class Router {
	private assignedRoutes: Route<unknown>[] = [];

	private assignRoute<T>(
		path: string,
		method: HTTPMethod,
		handler: Handler<T>
	): void {
		if (this.findRoute(path, method))
			throw new Error(`${path} is already assigned!`);

		this.assignedRoutes.push({
			path,
			method,
			handler,
		});
	}

	private findRoute(path: string, method: HTTPMethod): Route<unknown> | null {
		const route = this.assignedRoutes.find(
			(value) => value.path === path && value.method === method
		);

		return route === undefined ? null : route;
	}

	get<T>(path: string, handler: Handler<T>): void {
		this.assignRoute(path, HTTPMethod.GET, handler);
	}

	post<T>(path: string, handler: Handler<T>): void {
		this.assignRoute(path, HTTPMethod.POST, handler);
	}

	put<T>(path: string, handler: Handler<T>): void {
		this.assignRoute(path, HTTPMethod.PUT, handler);
	}

	delete<T>(path: string, handler: Handler<T>): void {
		this.assignRoute(path, HTTPMethod.DELETE, handler);
	}

	getHandler() {
		return async (
			request: IncomingMessage & { body: object },
			response: ServerResponse
		) => {
			if (!request.url || !request.method) {
				return endErrorRequest(400, "Incorrect request object", response);
			}
			const route = this.findRoute(request.url, request.method as HTTPMethod);

			// If such route not found in handlers -> respond with 404
			if (!route) return endErrorRequest(404, "Not Found", response);

			try {
				let rawBody: string = "";
				let body: object = {};

				request.on("data", (chunk: string) => {
					rawBody += chunk;
				});

				request.on("end", async () => {
					body = JSON.parse(rawBody);
					request.body = body;

					try {
						return await route.handler(request, response);
					} catch (e) {
						console.log(e);
						endErrorRequest(500, "Internal Server Error", response);
					}
				});
			} catch (e) {
				console.log(e);
				endErrorRequest(500, "Internal Server Error", response);
			}
		};
	}
}

export type Handler<T> = (
	request: IncomingMessage,
	response: ServerResponse
) => T;

export enum HTTPMethod {
	GET = "GET",
	POST = "POST",
	PUT = "PUT",
	DELETE = "DELETE",
}

function endErrorRequest(
	statusCode: number,
	statusMessage: string,
	response: ServerResponse
) {
	response.statusCode = statusCode;
	response.statusMessage = statusMessage;

	response.end();
}
